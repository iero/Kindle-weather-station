#!/bin/sh

source kindle/bin/activate
cd "$(dirname "$0")"

python createSVG.py
convert -background white -depth 8 ieroStation.svg kindleStation.png
pngcrush -s -c 0 -ow kindleStation.png
cp -f kindleStation.png /var/www/html/weather/weather-script-output.png